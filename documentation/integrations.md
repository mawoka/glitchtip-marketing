---
title: GlitchTip Integrations
published: true
---
# Integrations

GlitchTip aims to be Sentry API compatible. Anything that works with Sentry should also work with GlitchTip. The following integrations have been verified to work. Please considering [adding more](https://gitlab.com/glitchtip/glitchtip-marketing/-/tree/master/documentation/integrations.md).

## GitLab Error Tracking

In GitLab, navigate to a project's Settings > Monitor > Error Tracking. Click to enable tracking. Select "Sentry" and enter the GlitchTip URL (for example, https://app.glitchtip.com). Get your Auth Token from GlitchTip by navigating to Profile > Auth Tokens. Note that Auth Tokens give access to any project your user has permission to. You may select all permission scopes or limit them (giving GitLab less access, such as read only access). Select Connect and then pick the Project you want to associate with the GitLab Project.

<img style="max-width: 400px" src="/assets/documentation/gitlab.png" alt="GitLab settings example"></img>
