import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ScullyLibModule } from '@scullyio/ng-lib';
import { LegalRoutingModule } from './legal-routing.module';
import { LegalComponent } from './legal.component';
import { MatCardModule } from '@angular/material/card';

@NgModule({
  declarations: [LegalComponent],
  imports: [CommonModule, LegalRoutingModule, ScullyLibModule, MatCardModule]
})
export class LegalModule {}
